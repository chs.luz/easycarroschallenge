Navegar atá a raiz do projeto utilizando o terminal
e rodar o comando

### `npm intall`

Para rodar o projeto utilizar o comando:

### `node -r esm src/server.js`

### `url base http://localhost:8080`

### `Serviços`



### `LOGIN`
Login: http://localhost:8080/api/login
informações necessárias
body: {
    "email":"DIGITE SEU EMAIL",
	"password":"DIGITE SUA SENHA"
}

header: {
    Content-Type: application/json
}

retorna: 
{
  "auth": true,
  "token": "TOKEN DE AUTENTICAÇÃO"
}


### `REQUISITAR SERVIÇO`
Login: http://localhost:8080/api/request-service
informações necessárias
body: {
	"service":"SERVICO",
	"cordinates": {
		"lat": LATITUDE,
		"long": LONGITUDE
	}
}

header: {
    Content-Type: application/json,
    x-access-token: TOKEN_DE_AUTENTICACAO_GERADO_PELO_LOGIN
}

retorna: 
{
  DADOS DO CLIENTE QUE PRESTA O SERVICO, SE ESTIVER DENTRO DO RAIO E O CLIENTE TIVER O SERVIÇO PRESTADO,
  OU ENTÂO RETORNA UMA MENSAGEM INFORMANDO QUE NINGUEM PRESTA O SERVIÇO NA AREA
}


### `LISTAR OS CLIENTES E SERVIÇOS A PARTIR DO NOME DE UMA RUA`
Login: http://localhost:8080/api/find-address
informações necessárias
body: {
	"address":"ex: Av. Paulista, 1578 - Bela Vista"
}

header: {
    Content-Type: application/json,
    x-access-token: TOKEN_DE_AUTENTICACAO_GERADO_PELO_LOGIN
}

retorna: 
{
  o nome de cada cliente e a lista dos serviços prestados, ou então uma lista vazia
}

