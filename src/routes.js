const express = require('express');

const AuthController = require('./controllers/AuthController');
const AddressController = require('./controllers/AddressController');
const ServicePartnerController = require('./controllers/ServicePartnerController');


const routes = express.Router();
routes.post('/api/find-address',AuthController.verifyJWT, AddressController.index);
routes.post('/api/request-service',AuthController.verifyJWT,ServicePartnerController.index)
routes.post('/api/login',AuthController.store);

module.exports = routes;