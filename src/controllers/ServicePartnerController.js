import Geohash from 'latlon-geohash';
import { loadDataPartiners } from '../data/loadData';

module.exports = {
    index(req,res) {
        const { service, cordinates } = req.body;
        let geohash = Geohash.encode(cordinates.lat,cordinates.long,5);
        let partners = loadDataPartiners();
        let partner = partners.find(p => {
            let add = false;
            let clientGeohash = Geohash.encode(p.location.lat,p.location.long,5);
            let neighbours = Geohash.neighbours(clientGeohash);
            Object.keys(neighbours).forEach(n => {
                if(neighbours[n] === geohash) {
                    add = true;
                }
            })
            return add && p.availableServices.includes(service);
        })
        if(partner) {
            res.json(partner)
        }
        else {
            res.status(200).send({"msg":"não há um profissional disponível."})
        }
    }
}