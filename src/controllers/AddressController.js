const { API_KEY} = require('../utils');
import Geohash from 'latlon-geohash';
import { loadDataPartiners } from '../data/loadData';

const googleMapsClient = require('@google/maps').createClient({
    key: API_KEY,
    Promise: Promise
});

function findPartnersInsideLocation(cordinates) {
    let geohash = Geohash.encode(cordinates.lat,cordinates.lng,5);
    let partners = loadDataPartiners();
    let partnersToReturn = [];
    partners.forEach(p => {
        let clientGeohash = Geohash.encode(p.location.lat,p.location.long,5);
        let add = false;
        let neighbours = Geohash.neighbours(clientGeohash);
        Object.keys(neighbours).forEach(n => {
            if(neighbours[n] === geohash) {
                add = true;
            }
        })
        if(add) {
            let novo = {};
            novo.nome = p.name;
            novo.servicos = p.availableServices;
            partnersToReturn.push(novo);
        }
    });
    return partnersToReturn;
}

module.exports = {
    async index (req, res) {
        let { address } = req.body;
        let response = 
            await googleMapsClient
            .geocode({ address })
            .asPromise();
        let results = response.json.results;
        let cordinates = results[0].geometry.location;
        let partnersToReturn = findPartnersInsideLocation(cordinates);
        res.json(partnersToReturn)
    }
}