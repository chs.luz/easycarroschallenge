const jwt = require('jsonwebtoken');
import { loadDataUser } from '../data/loadData';
module.exports = {
    verifyJWT(req, res, next) {
        var token = req.headers['x-access-token'];
        if (!token) return res.status(401).send({ auth: false, message: 'Nenhum token de autenticação fornecido' });
        jwt.verify(token, "SECRET", function(err, decoded) {
          if (err) return res.status(500).send({ auth: false, message: 'Falha ao autenticar o token, forneça outro token.' });
          req.userId = decoded.id;
          next();
        });
    },

    store(req, res){
      const { email, password } = req.body;
      let users = loadDataUser();
      let user = users.find(user => user.email === email && user.password === password);
      if(user) {
          var token = jwt.sign({ user,password }, "SECRET", {
            expiresIn: 900 // expira em 15 min
        });
        res.status(200).send({ auth: true, token: token });
      }
      else {
        res.status(401).send({error: 'Usuário ou Senha Inválidos'})
      }
    }
}