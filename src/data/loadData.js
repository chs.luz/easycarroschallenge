const fs = require('fs');

module.exports = {
    loadDataPartiners(){
        try {
          const jsonString = fs.readFileSync('./src/data/partners.json')
          const partners = JSON.parse(jsonString)
          return partners;
        } catch(err) {
          return [];
        }
    },
    loadDataUser(){
        try {
          const jsonString = fs.readFileSync('./src/data/users.json')
          const users = JSON.parse(jsonString)
          return users;
        } catch(err) {
          return [];
        }
      }
}